/**
 * Describes the resolve function `type` of a promise
 */
export declare type ResolveFunc<T = any> = (value: T) => void;
/**
 * Describes the function `type` that we pass to `task` wagon or events
 */
export declare type TaskFunc<T = any, Y = any> = (value: T) => Y;
/**
 * Describes the function `type` each wagon carries
 */
export declare type WagonFunc<T = any, Y = any> = (value: T) => Promise<Y>;
export declare class Train {
    private _wagons;
    private _onRepeat;
    private _repeatCounter;
    private _startingRepeatCounter;
    /**
     * If `true`, `isReady` returns `true` too
     */
    private _repeatOverride;
    private _currentParallelLoad;
    private _cancelled;
    private _repeating;
    private _reusable;
    private _running;
    private static _completedTrains;
    /**
     * The constructor should not be called directly.
     * Use `Train.new()` instead
     */
    private constructor();
    /**
     * Adds a `task` wagon to the train
     * @param func
     * @returns
     */
    task(func: TaskFunc): Train;
    /**
     * Adds a `promise` wagon to the train
     * @param func
     * @returns
     */
    promise(func: WagonFunc): Train;
    /**
     * Adds a `parallel` wagon to the train.
     * A `parallel` wagon carries 0 or more trains in an array
     * @param trains
     */
    parallel(trains: Train[]): Train;
    /**
     * Adds a console `log` wagon to the train.
     * If you pass a function, then the result of that function is logged instead.
     * This wagon does not propagate its value
     * @param text
     * @returns
     */
    log(text: string | TaskFunc<any, string>): Train;
    /**
     * Adds a `wait` wagon to the train.
     * If you pass a function, then the result of that function is waited instead.
     * This wagon does not propagate its value
     * @param duration - The duration to wait in milliseconds
     * @returns
     */
    wait(duration: number | TaskFunc<any, number>): Train;
    /**
     * Adds a `waitUntil` wagon to the train which resolves when the returned resolve function is called
     * @returns
     */
    waitUntil(): ResolveFunc;
    /**
     * Toggles the repeating for the train
     * @param repeatCount - The number of repeats this train should do. Defaults to `-1`: infinite, `0`: stop repeating
     * @returns
     */
    repeat(repeatCount?: number): Train;
    /**
     * Changes a train to be reusable, meaning that its wagons won't get cleared upon completion.
     * That means that you can run the train over and over again with the same wagons.
     * Don't forget to change it back to be unusable before your last run to enable recycling
     * @param state - Defaults to `true`
     * @returns
     */
    reusable(state?: boolean): Train;
    /**
     * Adds a function to be called when the train repeats.
     * There can be multiple `onRepeat` callbacks
     * @param func
     * @returns
     */
    onRepeat(func: TaskFunc): Train;
    /**
     * Runs the train! Choo choo!
     * Running the train while it is still in motion will return a promise resolving the train itself
     * @param startValue - Start the train with a predetermined starting value. Defaults to `true`
     * @returns
     */
    run(startValue?: any): Promise<any>;
    /**
     * Stops the execution of this train.
     * Note that this functionality may span over multiple frames.
     * It does not resolve already pending promises, but automatically resolves all wagons after the current one
     * @returns
     */
    stop(): Train;
    /**
     * Clears the train and recycles it.
     * Also stops the train if it is already running
     * @returns
     */
    clear(): Train;
    /**
     * This code block handles operations after all wagons are done
     * @param value
     * @returns
     */
    private _afterRun;
    /**
     * Handles the repeat operations of this train.
     * If it returns true, then the train is run again
     * @param value
     * @returns
     */
    private _handleRepeat;
    /**
     * Triggers callbacks for an event with the given value
     * @param event
     * @param value
     * @returns
     */
    private _triggerEvents;
    /**
     * Creates a future promise to be resolved later
     * @returns
     */
    private _createFuturePromise;
    /**
     * Validates a parameter so that the result always returns either the parameter or the return value of the parameter
     * @param param
     * @returns
     */
    private _validateFunctionOrValueParams;
    /**
     * Is this train ready to run?
     */
    get isReady(): Boolean;
    /**
     * Is this train running?
     */
    get isRunning(): Boolean;
    /**
     * Is this train repeating?
     */
    get isRepeating(): Boolean;
    /**
     * Retrieves a new `Train` and returns it
     * @returns
     */
    static new(): Train;
    /**
     * Recycles a `Train` to be reused later
     * @param train
     * @returns
     */
    private static _recycle;
}
