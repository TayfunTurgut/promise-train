"use strict";
// #region Type declarations
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Train = void 0;
// #endregion
class Train {
    /**
     * The constructor should not be called directly.
     * Use `Train.new()` instead
     */
    constructor() {
        // The wagons of this train
        this._wagons = [];
        // Train events
        this._onRepeat = [];
        // Repeating train variables
        this._repeatCounter = 0;
        this._startingRepeatCounter = 0;
        /**
         * If `true`, `isReady` returns `true` too
         */
        this._repeatOverride = false;
        // Parallel wagon variables
        this._currentParallelLoad = [];
        // Train states
        this._cancelled = false;
        this._repeating = false;
        this._reusable = false;
        this._running = false;
    }
    // #region PUBLIC METHODS
    /**
     * Adds a `task` wagon to the train
     * @param func
     * @returns
     */
    task(func) {
        return this.promise((value) => __awaiter(this, void 0, void 0, function* () { return func(value); }));
    }
    /**
     * Adds a `promise` wagon to the train
     * @param func
     * @returns
     */
    promise(func) {
        this._wagons.push(func);
        return this;
    }
    /**
     * Adds a `parallel` wagon to the train.
     * A `parallel` wagon carries 0 or more trains in an array
     * @param trains
     */
    parallel(trains) {
        return this.promise((value) => {
            this._currentParallelLoad = trains;
            return Promise.all(trains.map((t) => t.run(value))).then((values) => {
                this._currentParallelLoad = [];
                return Promise.resolve(values);
            });
        });
    }
    /**
     * Adds a console `log` wagon to the train.
     * If you pass a function, then the result of that function is logged instead.
     * This wagon does not propagate its value
     * @param text
     * @returns
     */
    log(text) {
        const func = this._validateFunctionOrValueParams(text);
        return this.task((value) => {
            console.log(func(value));
            return value;
        });
    }
    /**
     * Adds a `wait` wagon to the train.
     * If you pass a function, then the result of that function is waited instead.
     * This wagon does not propagate its value
     * @param duration - The duration to wait in milliseconds
     * @returns
     */
    wait(duration) {
        const func = this._validateFunctionOrValueParams(duration);
        return this.promise((value) => {
            return new Promise((resolve) => setTimeout(() => resolve(value), func(value)));
        });
    }
    /**
     * Adds a `waitUntil` wagon to the train which resolves when the returned resolve function is called
     * @returns
     */
    waitUntil() {
        const futurePromise = this._createFuturePromise();
        const { promise, resolve } = futurePromise;
        this.promise(() => promise);
        return (value) => resolve.bind(promise)(value);
    }
    /**
     * Toggles the repeating for the train
     * @param repeatCount - The number of repeats this train should do. Defaults to `-1`: infinite, `0`: stop repeating
     * @returns
     */
    repeat(repeatCount = -1) {
        this._startingRepeatCounter = repeatCount;
        this._repeatCounter = repeatCount;
        this._repeating = Boolean(repeatCount);
        return this;
    }
    /**
     * Changes a train to be reusable, meaning that its wagons won't get cleared upon completion.
     * That means that you can run the train over and over again with the same wagons.
     * Don't forget to change it back to be unusable before your last run to enable recycling
     * @param state - Defaults to `true`
     * @returns
     */
    reusable(state = true) {
        this._reusable = state;
        return this;
    }
    /**
     * Adds a function to be called when the train repeats.
     * There can be multiple `onRepeat` callbacks
     * @param func
     * @returns
     */
    onRepeat(func) {
        this._onRepeat.push(func);
        return this;
    }
    /**
     * Runs the train! Choo choo!
     * Running the train while it is still in motion will return a promise resolving the train itself
     * @param startValue - Start the train with a predetermined starting value. Defaults to `true`
     * @returns
     */
    run(startValue = true) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.isReady)
                return Promise.resolve(this);
            this._running = true;
            this._repeatOverride = false;
            return this._wagons.reduce((prev, next) => prev.then((value) => {
                const { _cancelled } = this;
                if (_cancelled)
                    return Promise.resolve(value);
                return next(value);
            }), Promise.resolve(startValue))
                .then((value) => this._afterRun(value));
        });
    }
    /**
     * Stops the execution of this train.
     * Note that this functionality may span over multiple frames.
     * It does not resolve already pending promises, but automatically resolves all wagons after the current one
     * @returns
     */
    stop() {
        if (!this.isRunning) {
            return this.clear();
        }
        this._cancelled = true;
        this.repeat(0);
        const { _currentParallelLoad } = this;
        if (_currentParallelLoad.length > 0) {
            this._currentParallelLoad.forEach((t) => t.stop());
        }
        return this;
    }
    /**
     * Clears the train and recycles it.
     * Also stops the train if it is already running
     * @returns
     */
    clear() {
        if (this.isRunning) {
            this.stop();
            return this;
        }
        else {
            this._cancelled = false;
            this.repeat(0);
        }
        this._currentParallelLoad = [];
        if (this._reusable)
            return this;
        this._wagons = [];
        this._onRepeat = [];
        Train._recycle(this);
        return this;
    }
    // #endregion
    // #region PRIVATE METHODS
    /**
     * This code block handles operations after all wagons are done
     * @param value
     * @returns
     */
    _afterRun(value) {
        return __awaiter(this, void 0, void 0, function* () {
            const { _repeating } = this;
            if (_repeating) {
                if (this._handleRepeat(value)) {
                    return this.run(value);
                }
            }
            this._running = false;
            this.clear();
            return Promise.resolve(value);
        });
    }
    /**
     * Handles the repeat operations of this train.
     * If it returns true, then the train is run again
     * @param value
     * @returns
     */
    _handleRepeat(value) {
        const { _repeatCounter, _startingRepeatCounter, _onRepeat } = this;
        const hasRepeatCounter = _repeatCounter > 0;
        const isInfinite = _startingRepeatCounter === -1;
        if (hasRepeatCounter || isInfinite) {
            if (hasRepeatCounter) {
                this._repeatCounter--;
            }
            this._triggerEvents(_onRepeat, value);
            this._repeatOverride = true;
            return true;
        }
        this._repeating = false;
        return false;
    }
    /**
     * Triggers callbacks for an event with the given value
     * @param event
     * @param value
     * @returns
     */
    _triggerEvents(event, value) {
        if (event.length === 0)
            return this;
        event.forEach((e) => e(value));
        return this;
    }
    /**
     * Creates a future promise to be resolved later
     * @returns
     */
    _createFuturePromise() {
        const futurePromiseHandler = {};
        futurePromiseHandler.promise = new Promise((resolve, reject) => {
            futurePromiseHandler.resolve = resolve;
            futurePromiseHandler.reject = reject;
        });
        return futurePromiseHandler;
    }
    /**
     * Validates a parameter so that the result always returns either the parameter or the return value of the parameter
     * @param param
     * @returns
     */
    _validateFunctionOrValueParams(param) {
        if (typeof param === 'function') {
            return param;
        }
        else {
            return () => param;
        }
    }
    // #endregion
    // #region GETTERS / SETTERS
    /**
     * Is this train ready to run?
     */
    get isReady() {
        const { _running, _repeatOverride } = this;
        return !_running || _repeatOverride;
    }
    /**
     * Is this train running?
     */
    get isRunning() {
        return this._running;
    }
    /**
     * Is this train repeating?
     */
    get isRepeating() {
        return this._repeating;
    }
    // #endregion
    // #region STATIC METHODS
    /**
     * Retrieves a new `Train` and returns it
     * @returns
     */
    static new() {
        const { _completedTrains } = this;
        if (_completedTrains.length > 0) {
            return this._completedTrains.pop();
        }
        return new Train();
    }
    /**
     * Recycles a `Train` to be reused later
     * @param train
     * @returns
     */
    static _recycle(train) {
        const { _completedTrains } = this;
        if (!_completedTrains.includes(train)) {
            _completedTrains.push(train);
        }
        return this;
    }
}
exports.Train = Train;
// Static properties
Train._completedTrains = [];
