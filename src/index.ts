// #region Type declarations

/**
 * Describes the resolve function `type` of a promise
 */
export type ResolveFunc<T = any> = (value: T) => void;
/**
 * Describes the function `type` that we pass to `task` wagon or events
 */
export type TaskFunc<T = any, Y = any> = (value: T) => Y;
/**
 * Describes the function `type` each wagon carries
 */
export type WagonFunc<T = any, Y = any> = (value: T) => Promise<Y>;
/**
 * Describes the interface `waitUntil` wagon uses.
 * It is not exported as it is used internally
 */
interface IFuturePromiseHandler {
  resolve: ResolveFunc;
  reject: ResolveFunc;
  promise: Promise<any>;
}

// #endregion

export class Train {
  // The wagons of this train
  private _wagons: WagonFunc[] = [];
  // Train events
  private _onRepeat: TaskFunc[] = [];
  // Repeating train variables
  private _repeatCounter: number = 0;
  private _startingRepeatCounter: number = 0;
  /**
   * If `true`, `isReady` returns `true` too
   */
  private _repeatOverride: boolean = false;
  // Parallel wagon variables
  private _currentParallelLoad: Train[] = [];
  // Train states
  private _cancelled: boolean = false;
  private _repeating: boolean = false;
  private _reusable: boolean = false;
  private _running: boolean = false;
  // Static properties
  private static _completedTrains: Train[] = [];
  /**
   * The constructor should not be called directly.
   * Use `Train.new()` instead
   */
  private constructor() {}

  // #region PUBLIC METHODS

  /**
   * Adds a `task` wagon to the train
   * @param func
   * @returns
   */
  task(func: TaskFunc): Train {
    return this.promise(async (value: any) => func(value));
  }

  /**
   * Adds a `promise` wagon to the train
   * @param func
   * @returns
   */
  promise(func: WagonFunc): Train {
    this._wagons.push(func);
    return this;
  }

  /**
   * Adds a `parallel` wagon to the train.
   * A `parallel` wagon carries 0 or more trains in an array
   * @param trains
   */
  parallel(trains: Train[]): Train {
    return this.promise((value: any) => {
      this._currentParallelLoad = trains;
      return Promise.all(trains.map((t) => t.run(value))).then((values: any[]) => {
        this._currentParallelLoad = [];
        return Promise.resolve(values);
      });
    });
  }

  /**
   * Adds a console `log` wagon to the train.
   * If you pass a function, then the result of that function is logged instead.
   * This wagon does not propagate its value
   * @param text
   * @returns
   */
  log(text: string | TaskFunc<any, string>): Train {
    const func = this._validateFunctionOrValueParams(text);
    return this.task((value: any) => {
      console.log(func(value));
      return value;
    });
  }

  /**
   * Adds a `wait` wagon to the train.
   * If you pass a function, then the result of that function is waited instead.
   * This wagon does not propagate its value
   * @param duration - The duration to wait in milliseconds
   * @returns
   */
  wait(duration: number | TaskFunc<any, number>): Train {
    const func = this._validateFunctionOrValueParams(duration);
    return this.promise((value: any) => {
      return new Promise((resolve) => setTimeout(() => resolve(value), func(value)));
    });
  }

  /**
   * Adds a `waitUntil` wagon to the train which resolves when the returned resolve function is called
   * @returns
   */
  waitUntil(): ResolveFunc {
    const futurePromise: IFuturePromiseHandler = this._createFuturePromise();
    const { promise, resolve } = futurePromise;
    this.promise(() => promise);
    return (value: any) => resolve.bind(promise)(value);
  }

  /**
   * Toggles the repeating for the train
   * @param repeatCount - The number of repeats this train should do. Defaults to `-1`: infinite, `0`: stop repeating
   * @returns
   */
  repeat(repeatCount: number = -1): Train {
    this._startingRepeatCounter = repeatCount;
    this._repeatCounter = repeatCount;
    this._repeating = Boolean(repeatCount);
    return this;
  }

  /**
   * Changes a train to be reusable, meaning that its wagons won't get cleared upon completion.
   * That means that you can run the train over and over again with the same wagons.
   * Don't forget to change it back to be unusable before your last run to enable recycling
   * @param state - Defaults to `true`
   * @returns
   */
  reusable(state: boolean = true): Train {
    this._reusable = state;
    return this;
  }

  /**
   * Adds a function to be called when the train repeats.
   * There can be multiple `onRepeat` callbacks
   * @param func
   * @returns
   */
  onRepeat(func: TaskFunc): Train {
    this._onRepeat.push(func);
    return this;
  }

  /**
   * Runs the train! Choo choo!
   * Running the train while it is still in motion will return a promise resolving the train itself
   * @param startValue - Start the train with a predetermined starting value. Defaults to `true`
   * @returns
   */
  async run(startValue: any = true): Promise<any> {
    if (!this.isReady) return Promise.resolve(this);
    this._running = true;
    this._repeatOverride = false;

    return this._wagons.reduce(
        (prev, next) => prev.then((value) => {
          const { _cancelled } = this;
          if (_cancelled) return Promise.resolve(value);
          return next(value);
        }),
        Promise.resolve(startValue),
      )
      .then((value) => this._afterRun(value));
  }

  /**
   * Stops the execution of this train.
   * Note that this functionality may span over multiple frames.
   * It does not resolve already pending promises, but automatically resolves all wagons after the current one
   * @returns
   */
  stop(): Train {
    if (!this.isRunning) {
      return this.clear();
    }
    this._cancelled = true;
    this.repeat(0);
    const { _currentParallelLoad } = this;
    if (_currentParallelLoad.length > 0) {
      this._currentParallelLoad.forEach((t) => t.stop());
    }
    return this;
  }

  /**
   * Clears the train and recycles it.
   * Also stops the train if it is already running
   * @returns
   */
  clear(): Train {
    if (this.isRunning) {
      this.stop();
      return this;
    } else {
      this._cancelled = false;
      this.repeat(0);
    }
    this._currentParallelLoad = [];
    if (this._reusable) return this;
    this._wagons = [];
    this._onRepeat = [];
    Train._recycle(this);
    return this;
  }

  // #endregion

  // #region PRIVATE METHODS

  /**
   * This code block handles operations after all wagons are done
   * @param value
   * @returns
   */
  private async _afterRun(value: any): Promise<any> {
    const { _repeating } = this;
    if (_repeating) {
      if (this._handleRepeat(value)) {
        return this.run(value);
      }
    }
    this._running = false;
    this.clear();
    return Promise.resolve(value);
  }

  /**
   * Handles the repeat operations of this train.
   * If it returns true, then the train is run again
   * @param value
   * @returns
   */
  private _handleRepeat(value: any): Boolean {
    const { _repeatCounter, _startingRepeatCounter, _onRepeat } = this;
    const hasRepeatCounter = _repeatCounter > 0;
    const isInfinite = _startingRepeatCounter === -1;
    if (hasRepeatCounter || isInfinite) {
      if (hasRepeatCounter) {
        this._repeatCounter--;
      }
      this._triggerEvents(_onRepeat, value);
      this._repeatOverride = true;
      return true;
    }
    this._repeating = false;
    return false;
  }

  /**
   * Triggers callbacks for an event with the given value
   * @param event
   * @param value
   * @returns
   */
  private _triggerEvents(event: TaskFunc[], value: any): Train {
    if (event.length === 0) return this;
    event.forEach((e) => e(value));
    return this;
  }

  /**
   * Creates a future promise to be resolved later
   * @returns
   */
  private _createFuturePromise(): IFuturePromiseHandler {
    const futurePromiseHandler: IFuturePromiseHandler = {} as IFuturePromiseHandler;
    futurePromiseHandler.promise = new Promise((resolve, reject) => {
      futurePromiseHandler.resolve = resolve;
      futurePromiseHandler.reject = reject;
    });
    return futurePromiseHandler;
  }

  /**
   * Validates a parameter so that the result always returns either the parameter or the return value of the parameter
   * @param param
   * @returns
   */
  private _validateFunctionOrValueParams<T>(param: T | TaskFunc<any, T>): TaskFunc<any, T> {
    if (typeof param === 'function') {
      return param as TaskFunc<any, T>;
    } else {
      return () => param;
    }
  }

  // #endregion

  // #region GETTERS / SETTERS

  /**
   * Is this train ready to run?
   */
  get isReady(): Boolean {
    const { _running, _repeatOverride } = this;
    return !_running || _repeatOverride;
  }

  /**
   * Is this train running?
   */
  get isRunning(): Boolean {
    return this._running;
  }

  /**
   * Is this train repeating?
   */
  get isRepeating(): Boolean {
    return this._repeating;
  }

  // #endregion

  // #region STATIC METHODS

  /**
   * Retrieves a new `Train` and returns it
   * @returns
   */
  static new(): Train {
    const { _completedTrains } = this;
    if (_completedTrains.length > 0) {
      return this._completedTrains.pop() as Train;
    }
    return new Train();
  }

  /**
   * Recycles a `Train` to be reused later
   * @param train
   * @returns
   */
  private static _recycle(train: Train): typeof Train {
    const { _completedTrains } = this;
    if (!_completedTrains.includes(train)) {
      _completedTrains.push(train);
    }
    return this;
  }

  // #endregion
}
