# Upcoming features (Also kind of a TODO list)

- More examples
- Unit testing
- Game engine integrations

# 3.4.0

Now if `stop` is called on a stack that was created but never run, it recycles automatically instead of doing nothing.

# 3.3.0

The `onComplete` event and all code relating to that was removed. It did not make sense for a sequencer to have a completion event since you can always chain functions with `then` or `await` its completion. 

# v3.2.0

Parallel wagons now properly propagate their values as arrays.
Also, stopping a train with parallel wagons now also stops all trains in that wagon.
Added .prettierrc to the project root to control Prettier behavior

## Examples

- Added parallel wagon values demonstration in the `parallel` example.

# v3.1.0

Streamlines `isReady` accessor so that it represents the correct state of the train.
Also, splits post completion processes into its own function for better readability.

# v3.0.0

Because a lot of the codebase changed with this update, there was a major version bump.

## Examples

- Parallel
- Repeat
- Reusable

## Wagons

- `parallel`

# v2.1.3

Adds documentation to the project

# v2.1.0

Adds the first round of functionality.

## Examples

- Hello world
- Wagon values

## Wagons

- `task`
- `promise`
- `log`
- `wait`
- `waitUntil`

## Modifications

- `repeat`
- `reusable`

## Events

- `onRepeat`

## Management

- `stop`
- `clear`

# v2.0.0

Publishes the package with an empty README.

# v1.0.0

SEE README for the information about the previous owner of this package.