import { Train } from '../out/index';

const nonReusableTrain: Train = Train.new();
nonReusableTrain.log('Non reusable train is finished!');
// Logs once
nonReusableTrain.run().then(() => {
  // Since the train is not reusable, there is no log because it was cleared. The run here is a no-op
  nonReusableTrain.run();
});

const reusableTrain: Train = Train.new();
// Mark the train as reusable
reusableTrain.reusable();
reusableTrain.wait(500);
reusableTrain.log((value: string) => `${value} reusable train run is finished!`);
// Logs twice overall
reusableTrain.run('First').then(() => {
  // Toggle reusability off to recycle it automatically
  reusableTrain.reusable(false);
  // Since the train is reusable, we can run it again
  reusableTrain.run('Second');
});
