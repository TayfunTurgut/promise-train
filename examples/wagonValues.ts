import { Train } from '../out/index';

const train: Train = Train.new();
train.task((value) => {
  console.log(`The default starting value of the train is: ${value}`);
  // This wagon returns a value of 4
  return 4;
});
// Will wait for the previous wagon's `value * 1000`
train.wait((value) => value * 1000);
// This logs the result (thus the value of the previous wagon yet again!)
train.log((value) => value);
// Let's multiply the previous wagon's value by 5
train.task((value) => value * 5);
train.log((value) => value);

// Logs `4, 20`
train.run();
