import { Train } from '../out/index';

// Create our new train
const train = Train.new();
// This train will repeat 4 times (runs a total of 5 times)
train.repeat(4);
// Log the value
train.log((value) => value);
// Just a task that returns previous wagon's value + 1
train.task((value) => value + 1);

// First value starts with 1, logs `2, 3, 4, 5, 6`
train.run(1);
