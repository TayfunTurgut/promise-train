import { Train } from '../out/index';

// Create new train
const train: Train = Train.new();
// Add a log wagon to the train
train.log('Hello');
// Wait for 1 second
train.wait(1000);
// Then log another string by joining the array below WHEN it is called
const worldLetters = ['w', 'o', 'r', 'l', 'd'];
train.log(() => worldLetters.join(''));
// So I can change the array before it is joined:
worldLetters.push('!');

// Logs `Hello world!`
train.run();

// Observe that `Here!` logs before `Hello`
console.log('Here!');
