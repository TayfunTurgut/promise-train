import { Train } from '../out/index';

// This is the train that will hold the parallel wagon
const mainTrain: Train = Train.new();
// The parallel wagon will carry this array
const parallelWagonLoad: Train[] = [];

for (let i = 0; i < 10; i++) {
  // Create new sub-train
  const subTrain: Train = Train.new();
  // Will wait for this duration
  const duration: number = i * 200;
  subTrain.wait(duration);
  subTrain.log(`This wagon waited for ${duration} milliseconds!`);
  // To propagate the value of this train, we need to use task as wait and log does not do so
  subTrain.task(() => duration);

  // Note that we don't `run` the train
  parallelWagonLoad.push(subTrain);
}

// Add the parallel wagon with 10 Trains
mainTrain.parallel(parallelWagonLoad);

mainTrain.log(
  (value: number[]) => `This is the result of the previous parallel wagon:\n[${value.join(', ')}]`,
);

// Waits and logs 10 lines with increasing durations
mainTrain.run();
