# promise-train

Welcome to Promise Train! 🚂 Glad to have you here 😻

You can find a detailed documentation hosted on Gitlab [here](https://tayfunturgut.gitlab.io/promise-train)! You can also see this README there too! :^)

## What is it?

Promise train is a class that can be used to chain operations such as promises, functions, logs and can wait for a duration or until a condition is met. It can also be used alongside a game engine and handle more complex operations like promisified tweens.

### It is a (power) tool

First and foremost, it is a tool. Although we give you many ways to manipulate trains, it is up to you to find ways to use it at its maximum capacity!

### The train analogy 🚈🚃🚃🚃

Although it is presumably pretty self-explanatory, promise-train uses a train analogy to express its functionality. This passage from [Wikipedia](https://en.wikipedia.org/wiki/Railroad_car) explains it quite well:

```text
A railroad car, railcar, railway wagon, railway carriage, railway truck, railwagon, railcarriage or railtruck, also called a train car, train wagon, train carriage or train truck, is a vehicle used for the carrying of cargo or passengers on a rail transport system (a railroad/railway). Such cars, when coupled together and hauled by one or more locomotives, form a train. Alternatively, some passenger cars are self-propelled in which case they may be either single railcars or make up multiple units.
```

Each newly instantiated instance of a `Train` represents one `locomotive` that we can add `wagons` to. Each `wagon` contains an instruction. We build the train one `wagon` at a time, and then run it. The instructions then can be executed starting from the front to the end.

The most interesting concept of `promise-train` is the parallel wagon. It enables us to run any number of trains as we want in parallel and then be able to await for those trains' completion too!

## Installation

You can see the NPM package and its details [here](https://www.npmjs.com/package/promise-train)!

```bash
npm i promise-train
```

Although the package is hosted at [UNPKG](https://unpkg.com/promise-train), we don't currently support loading it into a `script` tag because we don't use the `window` scope.

## Usage

### Syntax

```ts
// Your new train is ready to be built! 
const train: Train = Train.new();
```

Note that it is not `new Train`, and the reason is that we constantly recycle trains to be reused later!

### Examples

You can find more examples in the examples folder of the module.

You may see each example in action by running the following command from the root of the project.

```bash
npx ts-node examples/helloWorld.ts
```

Just change `helloWorld` to any example you want.

#### Wait between two actions

```ts
const train: Train = Train.new();
// We can log using the return value of a function
train.log(() => 'This is the first log!');
// Wait 1 second in between
train.wait(1000);
// Or we can directly log a string
train.log('This is the second log!');
train.run();
```

#### Promises and tasks

```ts
const train: Train = Train.new();
// A mock of a promise from an API
train.promise(() => users.getUserData('Melisa'));
// Each wagon gets the return / resolution value of the previous one
// Task wagon runs a function
train.task((user: User) => {
  user.changeHealth(-1);
});
train.run();
```

#### WaitUntil

```ts
const train: Train = Train.new();
train.log('Train is waiting!');
// The train will wait until resolve is called
const resolve: (value: any) => void = train.waitUntil();
resolve.log('Train is moving again!');
train.run();
// ... app logic ...
resolve();
// Observe that the train starts moving again!
```

## Contribution

This module was designed with new coders in mind, aiming to enable the use of promises and asynchronous programming in their projects. It is not by any mean the best / optimized code and is very much open to pull requests / feedbacks.

You can contribute to the project by opening issues and/or pull requests as I check the repository daily.

You can also contact me directly at *tyfn.trgt@gmail.com*

## Acknowledgements

This package was handed over to me by [Tuur Dutoit](https://github.com/TuurDutoit), who was the previous owner of a package with the same name. You can find the old source code [here](https://github.com/TuurDutoit/promise-train). I want to thank him for his warm-hearted approach to promise-train and for enabling me to share it with the rest of the world.